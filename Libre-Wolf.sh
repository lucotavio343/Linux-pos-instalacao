#!/bin/bash

sudo apt update -y
sudo apt install wget apt-transport-https gnupg2 ubuntu-keyring -y
wget -O- https://deb.librewolf.net/keyring.gpg | sudo gpg --dearmor | sudo tee /usr/share/keyrings/librewolf.gpg
echo deb [arch=amd64 signed-by=/usr/share/keyrings/librewolf.gpg] http://deb.librewolf.net $(lsb_release -cs) main | sudo tee /etc/apt/sources.list.d/librewolf.list
sudo apt update -y
sudo apt install librewolf -y
