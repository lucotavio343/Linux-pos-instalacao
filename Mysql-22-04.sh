#!/bin/bash

sudo apt-get update -y
sudo apt-get install -y curl
curl -L -o mysql-apt-config_0.8.23-1_all.deb https://www.dropbox.com/s/dprvgvgr9pzidvq/mysql-apt-config_0.8.23-1_all.deb?dl=0

sudo dpkg -i mysql-apt-config_0.8.23-1_all.deb
sudo apt --fix-broken install -y
sudo apt-get update -y
sudo apt-get install mysql-server
